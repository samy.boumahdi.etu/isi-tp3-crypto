# Rendu "fichier de cartes bancaires"

    - Ibrahima Baldé
    - Samy Boumahdi

## Question 1

Déjà, pour le chiffrement nous procédons comme suit:

On a le responsable 1 qui a une clé usb1 protégée par un mot de passe et qui contient une clé de chiffrement A. Le responsable 2 qui a une clé usb2 protégée par un mot de passe et qui contient une clé de chiffrement B.

- Pour chiffrer le fichier carte bancaire, on combine la clé de chiffrement A avec la clé de chiffrement B pour obtenir une clé de chiffrement combinée C qu'on va utiliser pour crytper notre fichier à l’aide d'un algorithme ( AES par exemple ). Ainsi, on obtient un fichier de carte bancaire chiffré. Puis nous cryptons et sauvegardons la clé de chiffrement combinée dans la ramdisk avec le mot de passe **admin**.

- Pour déchiffrer le fichier, on déchiffre la clé de chiffrement C, il nous faut connaître l’algorithme préalablement utilisé et le mot de passe admin. Puis, on prends le fichier de carte bancaire on le déchiffre avec la clé de chiffrement C à l’aide de l’algorithme. Ainsi on obtient en clair le fichier de départ.

- clé usb1 et mdp1.
  clé de chiffrement A et un algo (aes)

- clé usb2 et mdp2.
  clé de chiffrement B et un algo(aes )

- Pour créer un responsable ou son adjoint, on execute le fichier **exp.sh** tout en modifiant le chemin.

- Pour la mise en service, on execute le fichier **mise_en_service.sh**

## Question 2

Pour ajouter une paire : On execute le fichier **ajouter_pair.sh**.

Pour supprimer une paire : On execute le fichier **deletePair.sh**.

Pour chercher les n° de cartes associées à un nom : On execute le fichier **search_card.sh**.

## Question 3

Le représentant A chargé de substituer le responsable A aura la même clé de chiffrement A que lui. Cependant cette clé de chiffrement A sera dans une clé usb3 (transmise au représentant A) protégée par un nouveau mot de passe.

Le représentant B chargé de substituer le responsable B aura la même clé de chiffrement B que lui. Cependant cette clé de chiffrement B sera dans une clé usb4 (transmise au représentant B) protégée par un nouveau mot de passe.

On a donc en tout 4 clés usb, 2 d'entre elles ont une clé de chiffrement A et les 2 autres une clé de chiffrement B. On a 4 mots de passe différents permettant de protéger l'accès au contenu des 4 clés usb.

On donne la même clé au responsble et représentant qui lui est attribué car dans tous les cas ils doivent pouvoir faire le meme décryptage peu importe la clé donc autant donner la même.

## Question 5

Supposons qu'on retire la responsabilité du responsable juridique. On va créer une clé de chiffrement A pour l'attribuer au nouveau responsable juridique qui lui, choisira son propre mot de passe ensuite on attribuera la même clé de chiffrement A à son representant avec son mot de passe inchangé.

Le responsable technique et son représentant gardent la même clé car la répudiation ne les concerne pas dans cet exemple.

Ensuite on peut re crypter nos fichiers avec la nouvelle clé C qui est la combinaison des clés de chiffrement A et B .
