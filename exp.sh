#!/bin/bash

# Exemple simplifié avec un seul responsable. (init.sh)

# 1) Le responsable insère sa clé usb, elle est dispo sous usb/

# 2) On génère une clé aléatoirement
dd if=/dev/random bs=32 count=1 > ramdisk/key.clear


# 3) Le responsable choisit un mot de passe pour protéger sa clé. La clé protégée est copiée sur sa clé usb
#openssl enc -pbkdf2 -aes-256 -in ramdisk/key.clear -out usb/key.enc
#Pour decrypter
#openssl enc -pbkdf2 -aes-256-cbc -d -in ramdisk/key.clear -out ./responsableJuridique/key.enc
openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/key.clear -out representantTechnique/key.enc

# openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/key.clear -out responsableTechnique/key.enc

# openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/key.clear -out representantJuridique/key.enc

# openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/key.clear -out responsableJuridique/key.enc

# On supprime la clé en clair
rm ramdisk/key.clear