reponse=0;
while  [ $reponse != 1 ] && [ $reponse != 2 ]
do echo "Tapez 1 si vous êtes le responsable juridique  ou tapez 2 si vous êtes son representant ? "
  read reponse
done
echo "${reponse}"
if [ $reponse -eq 1 ]
then echo "Entrez le mot de passe pour le responsable juridique"
      read passwd1
      cle1=`openssl enc -pbkdf2 -aes-256-cbc -d -in responsableJuridique/keyChiffrement.enc -k $passwd1`
else echo "Entrez le mot de passe du representant juridique "
      read passwd1
      cle1=`openssl enc -pbkdf2 -aes-256-cbc -d -in representantJuridique/keyChiffrement.enc -k $passwd1`
      #openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/key.clear -out responsableJuridique/key.enc

fi

reponse=0;
while  [ $reponse != 1 -a $reponse != 2 ]
do echo "Tapez 1 si vous êtes le responsable technique  ou tapez 2 si vous êtes son representant ? "
  read reponse
done
if [ $reponse -eq 1 ]
then echo "Entrez le mot de passe pour le responsable technique"
      read passwd2
      cle2=`openssl enc -pbkdf2 -aes-256-cbc -d -in responsableTechnique/key2Chiffrement.enc -k $passwd2`
else echo "Entrez le mot de passe du representant technique "
      read passwd2
      cle2=`openssl enc -pbkdf2 -aes-256-cbc -d -in representantTechnique/key2Chiffrement.enc -k $passwd2`
fi
cle3="$cle1$cle2"
# On crée et cryte le fichier d'information de carte bancaire
touch informationClient
openssl enc -pbkdf2 -aes-256-cbc -in informationClient -out infoCarte.enc -k $cle3
#openssl enc -pbkdf2 -aes-256-cbc -in cryptage1.enc -out infoCarte.enc -k $cle2

# clé en clair
# echo $cle1 > ramdisk/cle1.enc
# echo $cle2 > ramdisk/cle2.enc
echo $cle3 > ramdisk/cleFinal.enc

# Encryptage des clés
openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/cleFinal.enc -out ramdisk/cleChiffrement.enc -k "admin"
#openssl enc -pbkdf2 -aes-256-cbc -in ramdisk/cle2.enc -out ramdisk/cleChiffrement2.enc -k $passwd2 


rm -rf informationClient
rm -rf cryptage1.enc
rm -rf ramdisk/cleFinal.enc
# rm -rf ramdisk/cle1.enc
# rm -rf ramdisk/cle2.enc

echo "done"
