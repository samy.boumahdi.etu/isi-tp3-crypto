#!/bin/bash

echo "Saisir le nom à supprimer"
read nom
echo "le numéro  de carte bancaire à supprimer"
read carteBancaire
lineToDelete="${nom}  ${carteBancaire}"

# cleDechiffrement2=`cat ramdisk/cle2`
# cleDechiffrement1=`cat ramdisk/cle1`

# dechiffrement de la clé

openssl enc -pbkdf2 -aes-256-cbc -d -in ramdisk/cleChiffrement.enc -out key_value.enc -k "admin"



# decryptage
openssl enc -pbkdf2 -aes-256-cbc -d -in infoCarte.enc -out informationClient -k key_value.enc

grep -wq "${lineToDelete}" "informationClient"
if [ $? = 0 ]
then
  sed "/$lineToDelete/d" informationClient > informationClientTmp
  mv informationClientTmp informationClient

  # openssl enc -pbkdf2 -aes-256-cbc -in informationClient -out cryptage1.enc -k $cleDechiffrement1
  # openssl enc -pbkdf2 -aes-256-cbc -in cryptage1.enc -out infoCarte.enc -k $cleDechiffrement2
  openssl enc -pbkdf2 -aes-256-cbc -in informationClient -out infoCarte.enc -k key_value.enc

  echo "Information suprimée"
else
  echo "Information non trouvée"
fi
rm -rf informationClient
rm -rf informationClientTmp

